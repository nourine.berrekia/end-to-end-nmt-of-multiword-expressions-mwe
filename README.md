# Neural Machine Translation of Medical Multiword Expressions (MWE)

This is an end-to-end neural machine translation system (NMT) of multiword expressions (MWE).

## 1. first_analysis_corpus_EMEA.ipynb 

Clean and explore the EMEA TMX (Moses) corpus (European Medicines Agency) with Pandas regex patterns, Pandas and numpy function to delete NaN values, and resize the corpus with expressions fequency 1 to 5. The english or french expressions with 5 frequencies have variantions in terms of words and grammatical structure.

## 2. revision_and_deeper_analysis.ipynb 

Revise the corpus explored in the first program and clean the corpus with new pandas regex patterns.

## 3.  MWE_POS_patterns.ipynb 

Explre the grammatical and linguistic features using spaCy ("https://spacy.io/usage/linguistic-features"). We find out that the dataset linguistic features are mainly :

- Compound Nouns or nominal groups, such as: Change in baseline or Modification de la base de référence.          
- Named Entities,  such as : Ireland Boehringer and Ingelheim Ireland Ltd
- Collocations, these are frequent association from one word to another within a sentence(https:// fr.wikipedia.org/wiki/Collocation_(linguistique)).
- Sentences in question, affirmative, negative and imperative forms

## 4. Model Evaluation 
    
Model training and testing starting with simple keras encoder-decoder LSTM architecture.In the second part, we use a backend tensorflow model _2.3_ with an attention layer. 

- Particular focus on loss values during testing and training.
- Counting the BLEU score of the baseline. The latter is trained on a corpus of 8000 expressions and tested on 2000 expressions. The generated score is 25.

- In fact the corpus is not that large but in a second evaluation with a 31000 expression the BLEU score is fairly increased to 0.28.
- The score BLEU reaches 0.30 when we trained the model on the full corpus DATASET_2_to_10_revised

I was inspired directly by the tensorflow API, particularly the inference model : 
(https://www.tensorflow.org/tutorials/text), nmt_with_attention and one of the kaggle works (https://www.kaggle.com/).


This will be done after reading the dataframe object and cleaning the corpus *exploratory_emea_corpus* as explained also in the *revise_data*.

## References

* Ratchel Tatman. 2019. Evaluating Text Output in NLP: BLEU at your own risk (https://towardsdatascience.com/evaluating-text-output-in-nlp-bleu-at-your-own-risk-e8609665a213).
* Dzmitry Bahdanau, Kyunghyun Cho, and Yoshua Bengio. 2015. Neural machine translation by jointly learning to align and translate. ICLR.
* Ilya Sutskever, Oriol Vinyals, and Quoc V. Le. 2014. Sequence to sequence learning with neural networks. NIPS.
